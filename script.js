var caseInsensitive;
var matchInclusive;
var blockedTags;
var blockedPostsCounter = 0;

function removeArticle() {
	const articles = document.getElementsByTagName('article');
	for (let article of Array.from(articles)) {
		const postTags = article.getElementsByClassName('post-tag');
		for (let tag of Array.from(postTags)) {
			const anchors = tag.getElementsByTagName('a');
			for (let anchor of Array.from(anchors)) {
				const label = caseInsensitive ? anchor.innerHTML.toLowerCase() : anchor.innerHTML;
				if (article.style.display != 'none') {
					if (blockedTags.indexOf(label) > -1) {
						article.style.display = 'none';
						blockedPostsCounter += 1;
						console.log('removed article by tag', label);
					}
					if (matchInclusive) {
						for (var blockedTag of blockedTags) {
							if (blockedTag.length >= 3 && label.includes(blockedTag)) {
								article.style.display = 'none';
								blockedPostsCounter += 1;
								console.log('removed article by tag', label);
							}
						}
					}
				}
			}
		}
	}
}

function removeSection() {
	for (let tag of blockedTags) {
		if (caseInsensitive) {
			tag = tag.toLowerCase();
		}
		const sections = document.getElementsByClassName('label section');
		for (let section of Array.from(sections)) {
			var label = caseInsensitive ? section.innerHTML.toLowerCase() : section.innerHTML;
			var label = label.substring(8); //removes the <!---->
			if (label == tag) {
				section.parentElement.style.display = 'none';
			}
			if (matchInclusive) {
				if (tag.length >= 3 && label.includes(tag)) {
					section.parentElement.style.display = 'none';
				}
			}
		}
	}
}

function increaseBlockedPostsCounter() {
	chrome.storage.sync.set({ nineGagTagBlock: result.nineGagTagBlock });
}

function readdSection() {
	const sections = document.getElementsByClassName('label section');
	for (let section of Array.from(sections)) {
		if (section.parentElement.style.display == 'none') {
			var label = section.innerHTML.substring(8);
			if (!caseInsensitive && blockedTags.map((tag) => tag.toLowerCase()).indexOf(label.toLowerCase()) == -1) {
				section.parentElement.style.display = 'block';
			}
		}
	}
}

function readdArticle() {
	const articles = document.getElementsByTagName('article');
	for (let article of Array.from(articles)) {
		if (article.style.display == 'none') {
			const postTags = article.getElementsByClassName('post-tag');
			const articleTags = [];
			for (let tag of Array.from(postTags)) {
				const anchors = tag.getElementsByTagName('a');
				for (let anchor of Array.from(anchors)) {
					const label = anchor.innerHTML.toLowerCase();
					articleTags.push(label);
				}
			}
			var intersection = blockedTags.map((tag) => tag.toLowerCase()).filter((e) => articleTags.includes(e));
			if (!caseInsensitive && !intersection.length) {
				article.style.display = 'block';
			}
		}
	}
}
function run() {
	MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		var observer = new MutationObserver(function (mutations, observer) {
			caseInsensitive = result.nineGagTagBlock.caseInsensitive;
			matchInclusive = result.nineGagTagBlock.matchInclusive;
			blockedPostsCounter = result.nineGagTagBlock.blockedPostsCounter;
			if (!blockedTags) return;
			if (caseInsensitive) {
				blockedTags = blockedTags.map((tag) => tag.toLowerCase());
			}
			removeArticle();
			removeSection();
			readdSection();
			readdArticle();
			if (result.nineGagTagBlock.blockedPostsCounter != blockedPostsCounter) {
				result.nineGagTagBlock.blockedPostsCounter = blockedPostsCounter;
				chrome.storage.sync.set({ nineGagTagBlock: result.nineGagTagBlock });
			}
		});
		observer.observe(document, {
			childList: true,
			subtree: true,
			attributes: true,
		});
	});
}
run();
setInterval(() => {
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		blockedTags = result.nineGagTagBlock.tags;
		caseInsensitive = result.nineGagTagBlock.caseInsensitive;
		matchInclusive = result.nineGagTagBlock.matchInclusive;
		blockedPostsCounter = result.nineGagTagBlock.blockedPostsCounter;
	});
}, 1000);

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
	console.log(message);
	alert(message);
});
