function chipInputEvent(event) {
	if (event.key === 'Enter') {
		const value = event.srcElement.value;
		createChip(value);
		event.srcElement.value = '';
		chrome.storage.sync.get('nineGagTagBlock', function (result) {
			if (!result.nineGagTagBlock) {
				result = {
					nineGagTagBlock: {
						tags: [],
					},
				};
			}
			result.nineGagTagBlock.tags.push(value);
			chrome.storage.sync.set({ nineGagTagBlock: result.nineGagTagBlock });
		});
	}
}

function chipClickEvent(element, value) {
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		if (!result.nineGagTagBlock.tags) {
			return;
		} else if (Array.isArray(result.nineGagTagBlock.tags)) {
			var index = result.nineGagTagBlock.tags.indexOf(value);
			if (index !== -1) {
				result.nineGagTagBlock.tags.splice(index, 1);
			}
			chrome.storage.sync.set({ nineGagTagBlock: result.nineGagTagBlock }, (result) => {
				element.remove();
			});
		}
	});
}

function createChip(value) {
	const chipEl = document.createElement('div');
	const chipSpan = document.createElement('span');
	const chipIconEl = document.createElement('div');
	chipIconEl.classList.add('chip-icon');
	chipEl.classList.add('chip');
	chipSpan.innerHTML = value;
	chipIconEl.innerHTML = '&Cross;';
	chipIconEl.addEventListener('click', (event) => chipClickEvent(chipIconEl, value));
	chipEl.addEventListener('click', (event) => chipClickEvent(chipEl, value));
	chipEl.appendChild(chipSpan);
	chipEl.appendChild(chipIconEl);
	chipInput.parentNode.insertBefore(chipEl, chipInput);
	chrome.runtime.sendMessage('chip created', (response) => {
		// 3. Got an asynchronous response with the data from the service worker
		console.log('received user data', response);
	});
}

function switchCaseInsensitive(event) {
	const checked = event.srcElement.checked;
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		result.nineGagTagBlock.caseInsensitive = checked;
		chrome.storage.sync.set({ nineGagTagBlock: result.nineGagTagBlock });
	});
}

function switchMatchInclusive(event) {
	const checked = event.srcElement.checked;
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		result.nineGagTagBlock.matchInclusive = checked;
		chrome.storage.sync.set({ nineGagTagBlock: result.nineGagTagBlock });
	});
}

function setCheckbox() {
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		document.getElementById('caseInsensitive').checked = result.nineGagTagBlock.caseInsensitive;
		//document.getElementById('matchInclusive').checked = result.nineGagTagBlock.matchInclusive;
	});
}

function loadTags() {
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		const tags = result.nineGagTagBlock.tags;
		if (!tags) return;

		for (let tag of tags) {
			createChip(tag);
		}
	});
}

function setCounter() {
	chrome.storage.sync.get('nineGagTagBlock', function (result) {
		const counter = result.nineGagTagBlock.blockedPostsCounter || 0;
		blockedPostsCounter = document.getElementById('blockedPostsCounter');
		blockedPostsCounter.innerHTML = counter;
	});
}

document.addEventListener('DOMContentLoaded', () => {
	loadTags();
	setCheckbox();
	setCounter();
	chipInput = document.getElementById('chip-input');
	chipInput.addEventListener('keyup', chipInputEvent);

	caseInsensitiveInput = document.getElementById('caseInsensitive');
	caseInsensitiveInput.addEventListener('click', switchCaseInsensitive);
	// matchInclusiveInput = document.getElementById('matchInclusive');
	// matchInclusiveInput.addEventListener('click', switchMatchInclusive);
});
